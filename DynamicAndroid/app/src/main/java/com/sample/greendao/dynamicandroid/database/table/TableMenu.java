package com.sample.greendao.dynamicandroid.database.table;

import com.sample.greendao.dynamicandroid.database.framework.Table;

/**
 * Created by c_jsbustamante on 9/6/2017.
 */

public class TableMenu extends Table {

    // THIS IS A SAMPLE REPOSITORY
    private static final String TABLE_NAME = "tbl_menus";
    private static final String ID = "_id";
    private static final String OBJECT_ID = "object_id";
    private static final String MENU_NAME = "menu_name";
    private static final String MENU_TEXT = "menu_text";
    private static final String IS_ACTIVE = "is_active";
    private static final String TABLE_STRUCTURE = "CREATE TABLE " + TABLE_NAME + " (" +
            ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," + OBJECT_ID + " TEXT, " +
            MENU_NAME + " TEXT, " + MENU_TEXT + " TEXT, " + IS_ACTIVE + " TEXT " + ");";

    @Override
    public String getTableStructure() {
        return TABLE_STRUCTURE;
    }

    @Override
    public String getName() {
        return TABLE_NAME;
    }
}
