package com.sample.greendao.dynamicandroid.database.table;

import com.sample.greendao.dynamicandroid.database.framework.Table;

/**
 * Created by c_jsbustamante on 9/6/2017.
 */

public class TableObject extends Table {

    private static final String TABLE_NAME = "tbl_objects";
    private static final String ID = "_id";
    private static final String OBJECT_VALUE = "object_value";
    private static final String OBJECT_DESCRIPTION = "object_description";
    private static final String IS_ACTIVE = "is_active";
    private static final String TABLE_STRUCTURE = "CREATE TABLE " + TABLE_NAME + " (" +
            ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " + OBJECT_VALUE + " TEXT, " +
            OBJECT_DESCRIPTION + " TEXT, " + IS_ACTIVE + " TEXT);";

    @Override
    public String getTableStructure() {
        return TABLE_STRUCTURE;
    }

    @Override
    public String getName() {
        return TABLE_NAME;
    }
}
