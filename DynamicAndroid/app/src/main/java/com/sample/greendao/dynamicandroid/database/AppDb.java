package com.sample.greendao.dynamicandroid.database;


import ph.com.unilab.ul_ojl.database.framework.EngineDatabase;

public class AppDb extends EngineDatabase {

    public final static String DB_NAME = "db_ojl.sqlite";
    public final static int DB_VERSION = 1;

    public AppDb() {
        super(DB_NAME, DB_VERSION, true);
    }
}
